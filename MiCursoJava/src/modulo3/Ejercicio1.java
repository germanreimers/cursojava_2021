package modulo3;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		float promedio;
	
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la primera nota");
		float nota1 = scan.nextFloat();
		System.out.println("Ingrese la segunda nota");
		float nota2 = scan.nextFloat();
		System.out.println("Ingrese la tercera nota");
		float nota3 = scan.nextFloat();
		promedio = (nota1 + nota2 + nota3)/3;
		
		if (promedio>=7) {
			System.out.println("Aprobado con "+promedio);
		}
		else {
			System.out.println("Desaprobado con "+promedio);
		}
		
	}

}
