package modulo3;
import java.util.*;
public class Ejercicio2 {

	public static void main(String[] args) {
		int numero;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un numero:");
		numero = scan.nextInt();
		float resto = numero%2;
		
		if(resto==0) {
			System.out.println("El numero es par");
		}
		else {
			System.out.println("El numero es impar");
		}
		
	}

}
