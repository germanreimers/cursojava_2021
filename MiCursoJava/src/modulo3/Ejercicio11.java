package modulo3;
import java.util.*;

public class Ejercicio11 {

	public static void main(String[] args) {
		char l;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar una letra:");
		l = scan.next().charAt(0);
		if (l=='a' || l=='e' || l=='i' || l=='o' || l=='u') {
			System.out.println(l+" es una vocal");
		}
		else {
			System.out.println(l+" es una consonante");
		}
	}

}
