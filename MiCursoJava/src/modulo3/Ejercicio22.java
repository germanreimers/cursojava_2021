package modulo3;

import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {
		char cat=0;
		int  sldbase, antig, n;
		double sldneto = 0;
		
		n = (int)Math.random()*2+1;
		switch(n){
		case 1: cat='a';
		case 2: cat='b';
		case 3: cat='c';
		}
			
		antig = (int)Math.random()*30+1;
		
		sldbase = (int)(Math.random()*10000)+1;
		if (cat=='a') 
			sldneto=sldbase+1000;
		if (cat=='b') 
			sldneto=sldbase+2000;
		if (cat=='c') 
			sldneto=sldbase+3000;
				
		if (antig>=1 && antig<=5) {
			sldneto= sldneto+(sldneto*0.05);
		}
		else if (antig>5 && antig<10) {
			sldneto=(float)(sldbase+(sldbase*0.1));
		}
		else {
			sldneto=(float)(sldbase+(sldbase*0.3));
		}
		System.out.println("Este empleado tiene:");
		System.out.println("Categoria: "+cat);
		System.out.println("Antiguedad: "+antig);
		System.out.println("Sueldo base: "+sldbase);
		System.out.println("El sueldo neto es: $"+sldneto);

	}

}
