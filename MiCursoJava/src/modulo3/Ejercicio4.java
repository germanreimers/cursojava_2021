package modulo3;
import java.util.*;
public class Ejercicio4 {

	public static void main(String[] args) {
		String categoria;
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese categoria a, b o c:");
		categoria = scan.next();
		
		if (categoria.equals("a")) {
			System.out.println("Hijo");
		}
		else if (categoria.equals("b")) {
			System.out.println("Padres");
		}
		else if (categoria.equals("c")) {
			System.out.println("Abuelos");
		}
		else {
			System.out.println("Categoria incorrecta");
		}
		
	}

}
