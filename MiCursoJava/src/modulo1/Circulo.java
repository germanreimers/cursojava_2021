package modulo1;

//******PRUEBA EN CLASE***********
public class Circulo {
	private int radio;
	
	public int getRadio() {
		return radio;
	}
		
	public float calcularArea() {
	return radio*radio*(float)Math.PI;
	}
	
	//constructor
	public Circulo() {
		radio = 2;
	}
	
	public Circulo(int radioA) {
		radio = radioA;
	}
	
	public void setRadio(int radioA) {
		radio = radioA;
	}
		
}
