package modulo1;

public class Test {

	public static void main(String[] args) {
		Circulo circulo1 = new Circulo();
		Circulo circulo2 = new Circulo(3);
		
		System.out.println(circulo1.calcularArea());
		System.out.println(circulo1.getRadio());
		System.out.println(circulo2.getRadio());
		
		circulo1.setRadio(10);
		System.out.println(circulo1.getRadio());
		circulo1.setRadio(23);
		System.out.println(circulo1.getRadio());
	}

}
