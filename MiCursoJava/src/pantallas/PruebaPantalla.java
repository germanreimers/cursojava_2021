package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;

public class PruebaPantalla {

	private JFrame frame;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PruebaPantalla window = new PruebaPantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PruebaPantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 451, 301);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Primer Trimestre");
		lblNewLabel.setBounds(10, 11, 96, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel2 = new JLabel("Segundo Trimestre");
		lblNewLabel2.setBounds(10, 36, 96, 14);
		frame.getContentPane().add(lblNewLabel2);
		
		textField1 = new JTextField();
		textField1.setBounds(116, 8, 86, 20);
		frame.getContentPane().add(textField1);
		textField1.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Tercer Trimestre");
		lblNewLabel_1.setBounds(10, 61, 96, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		textField2 = new JTextField();
		textField2.setBounds(116, 33, 86, 20);
		frame.getContentPane().add(textField2);
		textField2.setColumns(10);
		
		textField3 = new JTextField();
		textField3.setBounds(116, 58, 86, 20);
		frame.getContentPane().add(textField3);
		textField3.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setBounds(212, 100, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_2 = new JLabel("Final:");
		lblNewLabel_2.setBounds(10, 104, 46, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		textResultado = new JTextField();
		textResultado.setHorizontalAlignment(SwingConstants.CENTER);
		textResultado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textResultado.setBackground(SystemColor.inactiveCaption);
		textResultado.setBounds(116, 101, 86, 20);
		frame.getContentPane().add(textResultado);
		textResultado.setColumns(10);
		
		
		
	}
}
